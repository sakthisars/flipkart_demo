package flipkart;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;


public class Launch {
	@Test
	public void Launchflipkart() throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\nagarajansn\\Documents\\eclipse\\Projectday\\Chromedriver\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://flipkart.com/");
		driver.findElementByXPath("(.//*[normalize-space(text()) and normalize-space(.)='Back to top'])[1]/following::button[1]").click();
		Actions ac = new Actions(driver);
		ac.moveToElement(driver.findElementByXPath("(//span[@class='_1QZ6fC _3Lgyp8'])[1]")).perform();
		driver.findElementByLinkText("Mi").click();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.titleContains("Mi"));
		String actualTitle = driver.getTitle();
		if(actualTitle.contains("Mi"))
		{
			System.out.println("Verification successful"+actualTitle);
		} 
		else
		{
			System.out.println("Verification of page title failed"+"actual title is"+actualTitle);
		}
		driver.findElementByXPath("//div[text()='Newest First']").click();

		List <WebElement> product_list = driver.findElementsByXPath("//div[@class='_3wU53n']");

		System.out.println("how many web elements"+product_list.size());

		List<String> products = new ArrayList<String>();
		for(WebElement eachproduct_list : product_list)
		{
			products.add(eachproduct_list.getText());
			Thread.sleep(1000);
		}

		List <WebElement> price_list = driver.findElementsByXPath("//div[@class='_1vC4OE _2rQ-NK']");

		System.out.println("how many prices in list"+price_list.size());

		List<String> prices = new ArrayList<String>();
		for(WebElement eachprice_list : price_list)
		{
			prices.add(eachprice_list.getText());
			Thread.sleep(1000);
		}

		int size = product_list.size();
		for(int i=0;i<size-1;i++)
		{
			System.out.println("");
			System.out.println(products.get(i)+prices.get(i));
		}
		driver.close();
	}

}
